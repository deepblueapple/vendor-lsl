// BC vendor 1.1 (copy of 1.0 very slightly modified by Wayne Peters - DeepBlueApple Resident)
// Vendor script that may be used by a player to sell objects for BC coins.
// Initiated 1/8/21
// On first rez the script estabilishes owner name and configures.
// Once activated the vendor gives menu to casual buyer On Touch.
// Script checks buyer at the server for a valid BC meter profile
// and enough balance to cover price asked for the product on offer.
// IF FALSE then post fail message and return to default.
// IF TRUE then offer a menu of options (Buy, Cancel) to buyer.
// IF "Buy" then poll server to deduct current price from buyer account 
// and add price to owner account.
// For owner menu we have options to Name product, Price product, use Image,
// add a Message for buyer when buying and Activate sale.
// The vendor object's inventory must contain this script, a vendor image
// of product or logo (only one) and a single product object that the vendor
// will give to buyer on successful sale.


key HttpKey = NULL_KEY;
key agent = NULL_KEY;
list choices = [];//Add choices for owner and buyer separately
integer channel_dialog;
integer listen_id;
integer OwnerListenHandle;
integer hideLabel;
string item1;
string item2;
string item3;
string item4;//type
string item5;
string item6;
string item7;
string item8;
string itemx;
string owner;
string buyer;
integer price;
string label;
string desc;
string namex;

// WP - Can be removed, not used (but maybe used in web() function?)
// integer ListenHandle;
// string instr;
// string action;
// key name;

//web(){} function needs adding back by Talla



doHideLabel() {
	hideLabel = TRUE;
        llSetText("",<1,1,1>,1.0);
}

doShowLabel() {
	hideLabel = FALSE;
        llSetText("BC Vendor\nTouch to buy "+(string)label,<1,1,1>,1.0);
}


default
{
    on_rez(integer start_param)
    {
        llResetScript();
    }
    
    state_entry()
    {
        desc=llGetObjectDesc();
	if(desc == "" || desc == "Product Name,1,0") {
	  llOwnerSay("Before you can use this, click item you must set a product name label and price (1-1000)");
	  llSetObjectDesc("Product Name,1,0");
	  desc = "Product Name,1,0";
	}

        list n = llParseString2List(desc, [","], []);        
        label = llList2String(n, 0);
        price = llList2Integer(n, 1);
	hideLabel = llList2Integer(n, 2); // 1 or 0
	if(hideLabel == FALSE) {
	  doShowLabel();
	} else {
	  doHideLabel();
        }
    }
    
    touch_end(integer total_number) 
    { 
        agent = llDetectedKey(0);
        namex = llKey2Name(agent);
        owner = llKey2Name(llGetOwner());
        if(namex != owner){buyer=namex;}
        state giver;
    } 
    
 
}//default



state giver
{
    state_entry()
    {
    if(namex==owner){choices = ["Rename Label","Toggle Label","Cancel","Price"];}
    else{choices = ["Cancel","Buy"];}  
        
        channel_dialog = ( -1 * (integer)("0x"+llGetSubString((string)llGetKey(),-5,-1)) );        
        listen_id = llListen( channel_dialog, "", agent, "");
        string buyMsg = "Buy '" + label +"'.";
        llDialog(agent,buyMsg,choices,channel_dialog); 
        llSetTimerEvent(30.0);
    }
    listen(integer channel, string name, key id, string choice)
    {
    
       if (channel == 9)
       {    
           list data = llParseString2List(choice,[" "],[""]);

           if (llList2String(data, 0) == "price" )
           {   
               price=(integer)llList2Integer(data, 1);
               if(price >= 1 && price <= 1000)
                {
                    llSay(0,"The price is now "+(string)price+".");
                    llSetObjectDesc((string)label+","+(string)price+","+(string)hideLabel);//************?
                    llSleep(3);
                    llListenRemove(listen_id);
                    llListenRemove(OwnerListenHandle);
                    llSetTimerEvent(0.0);
                    state default;        
                }
                else
                {
                    llSay(0,"Bad Price! It must be between 1 and 1000 coins. Please try again.");  
                    llSleep(3);
                    llListenRemove(listen_id);
                    llListenRemove(OwnerListenHandle);
                    llSetTimerEvent(0.0);
                    llSetText("",<1,1,1>,1.0);
                    state default;         
                }
            }
if (llList2String(data, 0) == "label" )
           {   
               // [WP] wrapped in for-loop so label can now contain spaces
               integer x;
               integer y = llGetListLength(data);
               string newLabel = "";
               for(x = 0; x < y - 1; ++x) {
                   newLabel = newLabel + llList2String(data, x+1);
                   if(x < y - 2) { newLabel = newLabel + " ";}
                }


               integer len = llStringLength(label);
               if(len >= 5 && len <= 20 && llSubStringIndex(newLabel, ",") < 0)
                {
		    label = newLabel;
                    llSay(0,"The label is now "+(string)label+".");
                    llSetObjectDesc((string)label+","+(string)price+","+(string)hideLabel);//************?
                    llSleep(3);
                    llListenRemove(listen_id);
                    llListenRemove(OwnerListenHandle);
                    llSetTimerEvent(0.0);
                    state default;        
                }
                else
                {
                    llSay(0,"The label is too short, long or contains a comma (,) character. Use between 5 and 20 letters and avoid commas.");  
                    llSleep(3);
                    llListenRemove(listen_id);
                    llListenRemove(OwnerListenHandle);
                    llSetTimerEvent(0.0);
                    llSetText("",<1,1,1>,1.0);
                    state default;         
                }
            }

        }//channel 9
        else if(channel == channel_dialog) {
		if(choice == "Price")
		{ 
		    OwnerListenHandle = llListen(9,"","","");
		    llSay(0,"Owner can set a price paid for this product. Current price is "+(string)price+" (Example /9 price VALUE)");
		    llSetTimerEvent(30.0);    
		}
		else if(choice == "Rename Label")
		{ 
		    OwnerListenHandle = llListen(9,"","","");
		    llSay(0,"Owner can name the product on offer with a label. Current label is "+(string)label+" (Example /9 label NAME)");
		    llSetTimerEvent(30.0);    
	       }
	       else if (choice == "Buy")
	       {
		   llListenRemove(listen_id);
		   llListenRemove(OwnerListenHandle);           
		   state end;
	      }
              else if (choice == "Toggle Label") {
		if(hideLabel == FALSE) {
			doHideLabel();
			llSetObjectDesc((string)label+","+(string)price+",1");
			llSay(0, "Label is now hidden");
		} else {
			doShowLabel();
			llSetObjectDesc((string)label+","+(string)price+",0");
			llSay(0, "Label is now shown");
		}
			llListenRemove(listen_id);
			llListenRemove(OwnerListenHandle);
			llSetTimerEvent(0.0);
			state default;
	      }
	      else if (choice == "Cancel")
	      {
		  llListenRemove(listen_id);
		  llListenRemove(OwnerListenHandle);
		  state default;
	      }
        }

} 

  
    timer()
    {
        llListenRemove(listen_id);
        llListenRemove(OwnerListenHandle);
        llSetTimerEvent(0.0);
	llResetScript();
    }       

    on_rez(integer start_param)
    {
        llResetScript();
    }

}//state giver

state end
{

   state_entry()
   {
   
       //web(); removed above
   
   }

    http_response(key request_id, integer status, list metadata, string body)
    {

        if(HttpKey != request_id)
            return;

        if(status != 200)
        {
            llSay(0,"INVALID HTTP RESPONSE");
            HttpKey = NULL_KEY;
            return;
        }
        if (HttpKey == request_id){            
            list l = llParseString2List(body, [";"], []);
            integer i;
            integer len = llGetListLength(l);
            for (i = 0; i < len; i++){
          //llOwnerSay("item" + (string)i + "==>"+ llList2String(l, i));
            if(i == 1){item1 = llList2String(l, i);}//owner
            if(i == 2){item2 = llList2String(l, i);}//buyer
            if(i == 3){item3 = llList2String(l, i);}//flag
            if(i == 4){item4 = llList2String(l, i);}//type
            if(i == 5){item5 = llList2String(l, i);}//bal1
            if(i == 6){item6 = llList2String(l, i);}//lead
            if(i == 7){item7 = llList2String(l, i);}//bal2
            if(i == 8){item8 = llList2String(l, i);}//log 
                                        }//for
        }//if req_id
            if (item3 == "nf")
            {                
                llSay(0,"Sorry, You Must have BC Meter to use this vendor!");
                llSleep(3.00);
		llResetScript(); 
            }
            else
            {
              llGiveInventory(agent,llGetInventoryName(INVENTORY_OBJECT, 0));

               llSleep(1.00);
	       // WP: Example, intended for buyer meter: cineshow Resident,payout,Prey,52,
               llSay(-146551,(string)buyer+",payout,"+(string)item4+","+(string)item5+",");
               llSleep(1.00);
               // WP: Example, intended for seller meter: deepblueapple Resident,payout,Slave,5052,
	       llSay(-146551,(string)owner+",payout,"+(string)item6+","+(string)item7+",");
               llSay(0,(string)price+" BC coins taken for "+(string)label+". Enjoy!");
	       llResetScript();
            }//itemx
            
    }//http_response  



    on_rez(integer start_param)
    {
        llResetScript();
    }

}


